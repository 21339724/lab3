package ie.ul.shuhupdaphone.gui;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.TextView;

import ie.ul.shuhupdaphone.R;

public class InformationActivity extends Activity {

    TextView information;

    @Override
    public void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_information);

        SharedPreferences userPreferences;
        String name;

        userPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        name = userPreferences.getString(AppPreferences.KEY_USERNAME, "");

        this.information = (TextView) findViewById(R.id.information);
        if(name.equals("")){
            this.information.setText(R.string.username_def_text);
        } else {
            this.information.setText(name);
        }
    }

}

